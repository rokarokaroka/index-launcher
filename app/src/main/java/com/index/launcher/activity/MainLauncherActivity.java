package com.index.launcher.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.index.launcher.R;
import com.index.launcher.databinding.ActivityMainLauncherBinding;
import com.index.launcher.utilities.Constants;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class MainLauncherActivity extends AppCompatActivity implements View.OnClickListener{
    private ActivityMainLauncherBinding mBinding;
    private MaterialDialog mDialog;
    private Handler mHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initUi();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initUi();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mHandler.removeCallbacksAndMessages(null);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mHandler.removeCallbacksAndMessages(null);
    }

    private void initUi() {
        mBinding = ActivityMainLauncherBinding.inflate(getLayoutInflater());
        setContentView(mBinding.getRoot());

        Glide.with(this).load(R.drawable.main_bg).into(mBinding.launcherBg);

        Glide.with(this).load(R.drawable.fb_game).into(mBinding.rivClassRoom);
        Glide.with(this).load(R.drawable.fb_movie).into(mBinding.rivStudy);
        Glide.with(this).load(R.drawable.fb_music).into(mBinding.rivUMeet);
        Glide.with(this).load(R.drawable.fb_social).into(mBinding.rivPijar);
        Glide.with(this).load(R.drawable.fb_browser).into(mBinding.rivRumahBelajar);
        Glide.with(this).load(R.drawable.fb_xbmc).into(mBinding.rivPc);
        Glide.with(this).load(R.drawable.fb_nv).into(mBinding.rivZoom);
        Glide.with(this).load(R.drawable.fb_wcs).into(mBinding.rivRuangguru);
        Glide.with(this).load(R.drawable.fb_market).into(mBinding.rivWhatsapp);

        checkWifiConnection();
        checkLANConnection();

        mBinding.btnPc.setOnClickListener(this);
        mBinding.btnWhatsapp.setOnClickListener(this);
        mBinding.btnBrowser.setOnClickListener(this);
        mBinding.btnStudy.setOnClickListener(this);
        mBinding.btnZoom.setOnClickListener(this);
        mBinding.btnUMeet.setOnClickListener(this);
        mBinding.btnRuangguru.setOnClickListener(this);
        mBinding.btnRumahBelajar.setOnClickListener(this);
        mBinding.btnPijar.setOnClickListener(this);

        mBinding.rlAllApps.setOnClickListener(this);
        mBinding.rlSettings.setOnClickListener(this);
        mBinding.rlSpeedup.setOnClickListener(this);

        mBinding.iconBluetooth.setOnClickListener(this);
        mBinding.iconWifi.setOnClickListener(this);

        mHandler = new Handler(Looper.getMainLooper());
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {

                Calendar calendar = Calendar.getInstance();
                Date date = calendar.getTime();
                mBinding.tvDayString.setText(new SimpleDateFormat("EEEE", new Locale("in", "ID")).format(date.getTime()));
                mHandler.postDelayed(this, 1000);
            }
        },100);
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.btn_pc :
                showBootConfirmationDialog();
                break;

            case R.id.btn_whatsapp :
                intent = new Intent(this, WhatsAppWebViewActivity.class);
                intent.putExtra(Constants.EXTRA_WEBVIEW_URL, getResources().getString(R.string.whatsapp_url));
                startActivity(intent);
                break;


            case R.id.btn_browser :
                try {
                    Uri uri = Uri.parse("https://www.google.com");
                    intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                } catch(Exception e) {
                    Toast.makeText(this, "No browser found", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.rl_settings :
                intent = new Intent(Settings.ACTION_SETTINGS);
                startActivityForResult(intent, 0);
                break;

            case R.id.iconBluetooth :
                try {
                    startActivity(new Intent(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS));
                } catch (Exception e) {
                    Toast.makeText(this, "No bluetooth device found", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.iconWifi :
                try {
                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                } catch (Exception e) {
                    Toast.makeText(this, "No wifi device found", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.rl_speedup :
                try {
                    System.runFinalization();
                    Runtime.getRuntime().gc();
                    System.gc();
                    Toast.makeText(this, "Memory has been cleared", Toast.LENGTH_SHORT).show();
                } catch(Exception e) {
                    Toast.makeText(this, "Problem clearing up memory", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.rl_all_apps :
                intent = new Intent(this, AllAppsActivity.class);
                startActivity(intent);
                break;

            case R.id.btn_study :
                openAppByPackageName("com.indihomestudy.app");
                break;

            case R.id.btn_zoom :
                openAppByPackageName("us.zoom.videomeetings");
                break;

            case R.id.btn_u_meet :
                openAppByPackageName("id.umeetme.android.publik");
                break;

            case R.id.btn_pijar :
                openAppByPackageName("com.pijarsekolah.parent");
                break;

            case R.id.btn_rumah_belajar:
                openAppByPackageName("id.go.kemdikbud.belajar.apprumahbelajar");
                break;

            case R.id.btn_ruangguru :
                openAppByPackageName("com.ruangguru.livestudents");
                break;

            default:
                break;
        }
    }

    private void openAppByPackageName(String packageName) {
        Intent intent = getPackageManager().getLaunchIntentForPackage(packageName);
        if(intent == null) {
            Toast.makeText(this, "No app installed", Toast.LENGTH_SHORT).show();
        } else {
            try {
                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(this, "No app installed", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void showBootConfirmationDialog() {
        mDialog = new MaterialDialog.Builder(this)
                .title("Apakah anda yakin ingin masuk ke Mode PC ?")
                .content("Anda akan berpindah ke mode PC jika melanjutkan.\n Harap simpan semua pekerjaan Anda sebelum berpindah ke mode PC.\n " +
                        "Apakah Anda ingin melanjutkan ?")
                .positiveText("Pindah ke Mode PC")
                .negativeText("Batalkan")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    bootToDebian();
                                }
                            }
                ).build();

        mDialog.show();
    }

    private void bootToDebian(){
        final MaterialDialog bootDialog = new MaterialDialog.Builder(this)
                .title("Berpindah ke Mode PC")
                .content("Harap Menunggu...")
                .progress(true, 0)
                .cancelable(false)
                .build();

        bootDialog.show();


        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    ArrayList<String> commandLine = new ArrayList<String>();
                    commandLine.add("reboot update");//$NON-NLS-1$

                    Process process = Runtime.getRuntime().exec("reboot update");
                } catch (IOException e) {
                    bootDialog.dismiss();
                    Toast.makeText(MainLauncherActivity.this, "Error IO Exception " + e.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        }, 3000);
    }

    private void checkWifiConnection() {
        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (mWifi.isConnected()) {
            mBinding.iconWifi.setColorFilter(
                    ContextCompat.getColor(this, R.color.activeTopBarIconState),
                        android.graphics.PorterDuff.Mode.MULTIPLY);
            mBinding.iconWifi.setBackground(ContextCompat.getDrawable(this,R.drawable.bg_icon_round_active));
            mBinding.iconWifi.setActivated(true);

        } else {
            mBinding.iconWifi.setColorFilter(
                    ContextCompat.getColor(this, R.color.superwhite),
                    android.graphics.PorterDuff.Mode.MULTIPLY);
            mBinding.iconWifi.setBackground(ContextCompat.getDrawable(this,R.drawable.bg_icon_round));
            mBinding.iconWifi.setActivated(false);
        }
    }

    private void checkLANConnection() {
        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mLan = connManager.getNetworkInfo(ConnectivityManager.TYPE_ETHERNET);

        if (mLan.isConnected()) {
            mBinding.iconLan.setColorFilter(
                    ContextCompat.getColor(this, R.color.activeTopBarIconState),
                    android.graphics.PorterDuff.Mode.MULTIPLY);
            mBinding.iconLan.setBackground(ContextCompat.getDrawable(this,R.drawable.bg_icon_round_active));
            mBinding.iconLan.setActivated(true);

        } else {
            mBinding.iconLan.setColorFilter(
                    ContextCompat.getColor(this, R.color.superwhite),
                    android.graphics.PorterDuff.Mode.MULTIPLY);
            mBinding.iconLan.setBackground(ContextCompat.getDrawable(this,R.drawable.bg_icon_round));
            mBinding.iconLan.setActivated(false);
        }
    }

    private void checkBluetoothConnection() {
        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_ETHERNET);

        if (mWifi.isConnected()) {
            mBinding.iconLan.setColorFilter(
                    ContextCompat.getColor(this, R.color.activeTopBarIconState),
                    android.graphics.PorterDuff.Mode.MULTIPLY);
            mBinding.iconLan.setBackground(ContextCompat.getDrawable(this,R.drawable.bg_icon_round_active));
            mBinding.iconLan.setActivated(true);

        } else {
            mBinding.iconLan.setColorFilter(
                    ContextCompat.getColor(this, R.color.superwhite),
                    android.graphics.PorterDuff.Mode.MULTIPLY);
            mBinding.iconLan.setBackground(ContextCompat.getDrawable(this,R.drawable.bg_icon_round));
            mBinding.iconLan.setActivated(false);
        }
    }
}
