package com.index.launcher.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.index.launcher.R;
import com.index.launcher.adapter.AllAppsAdapter;
import com.index.launcher.databinding.ActivityAllAppsBinding;
import com.index.launcher.interfaces.AllAppsInterface;
import com.index.launcher.objects.AppInfo;
import com.index.launcher.utilities.Utils;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class AllAppsActivity extends AppCompatActivity implements View.OnClickListener, AllAppsInterface {
    private ActivityAllAppsBinding mBinding;
    private AllAppsAdapter mAdapter;
    private ArrayList<AppInfo> mData = new ArrayList<>();

    private final AsyncTask<Void, Void, AppInfo[]> mApplicationLoader = new GetAllAppsAsyncTask();
    private MaterialDialog mLoadingAppsDialog;
    private Handler mHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = ActivityAllAppsBinding.inflate(getLayoutInflater());
        setContentView(mBinding.getRoot());

        Glide.with(this).load(R.drawable.main_bg).into(mBinding.launcherBg);
        mLoadingAppsDialog = new MaterialDialog.Builder(this)
                .progress(true, 1)
                .content("Loading Apps")
                .build();

        mLoadingAppsDialog.show();

        mBinding.llAllAppsBack.setOnClickListener(this);
        initRecyclerView();
        mApplicationLoader.execute();

        mHandler = new Handler(Looper.getMainLooper());
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Calendar calendar = Calendar.getInstance();
                Date date = calendar.getTime();
                mBinding.tvAllAppsDayString.setText(new SimpleDateFormat("EEEE", new Locale("in", "ID")).format(date.getTime()));
                mHandler.postDelayed(this, 1000);
            }
        },100);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mHandler.removeCallbacksAndMessages(null);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mHandler.removeCallbacksAndMessages(null);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_all_apps_back :
                finish();
                break;
        }
    }

    private void initRecyclerView() {
        mAdapter = new AllAppsAdapter(mData, this);
        mBinding.rvAllApps.setAdapter(mAdapter);
        mBinding.rvAllApps.setLayoutManager(new GridLayoutManager(this, 6));

        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onAppCardClicked(AppInfo appInfo) {
        String packageName = appInfo.getPackageName();
        PackageManager pm = getPackageManager();
        Intent launchIntent = pm.getLaunchIntentForPackage(packageName);

        if(launchIntent == null) {
            launchIntent = pm.getLeanbackLaunchIntentForPackage(packageName);
        }

        Intent startApp = launchIntent;
        startActivity(startApp);
    }

    private class GetAllAppsAsyncTask extends AsyncTask<Void, Void, AppInfo[]> {
        @Override
        protected AppInfo[] doInBackground(Void... voids) {
            return Utils.loadApplications(AllAppsActivity.this).toArray(new AppInfo[0]);
        }

        @Override
        protected void onPostExecute(AppInfo[] apps) {
            List<AppInfo> appInfos = Arrays.asList(apps);
            Collections.sort(appInfos, new Comparator<AppInfo>() {
                @Override
                public int compare(AppInfo appInfo, AppInfo t1) {
                    return appInfo.getName().compareTo(t1.getName());
                }
            });

            mData.clear();
            mData.addAll(appInfos);
            mAdapter.notifyDataSetChanged();
            mLoadingAppsDialog.dismiss();
        }
    }
}
