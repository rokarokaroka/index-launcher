package com.index.launcher.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.index.launcher.R;
import com.index.launcher.databinding.ItemAppsCardBinding;

public class AllAppsViewHolder extends RecyclerView.ViewHolder {
    public LinearLayout llItemAppsRoot;
    public ImageView ivItemAppsThumbnail;
    public TextView tvItemAppsName;

    public AllAppsViewHolder(View v) {
        super(v);
        this.llItemAppsRoot = v.findViewById(R.id.ll_item_apps_root) ;
        this.ivItemAppsThumbnail = v.findViewById(R.id.iv_item_apps_thumbnail) ;
        this.tvItemAppsName = v.findViewById(R.id.tv_item_apps_name) ;
    }
}
