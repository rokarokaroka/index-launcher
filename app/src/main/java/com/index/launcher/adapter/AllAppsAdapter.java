package com.index.launcher.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.index.launcher.R;
import com.index.launcher.databinding.ActivityAllAppsBinding;
import com.index.launcher.databinding.ItemAppsCardBinding;
import com.index.launcher.interfaces.AllAppsInterface;
import com.index.launcher.objects.AppInfo;
import com.index.launcher.viewholder.AllAppsViewHolder;

import java.util.ArrayList;

public class AllAppsAdapter extends RecyclerView.Adapter implements View.OnClickListener{
    private ItemAppsCardBinding mBinding;
    private ArrayList<AppInfo> mData;
    private AllAppsInterface mCallback;

    public AllAppsAdapter(ArrayList<AppInfo> appInfos, AllAppsInterface callback) {
        mData = appInfos;
        mCallback = callback;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_apps_card, parent, false);
        return new AllAppsViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        AllAppsViewHolder allAppsViewHolder = (AllAppsViewHolder) holder;
        allAppsViewHolder.llItemAppsRoot.setTag(position);
        allAppsViewHolder.llItemAppsRoot.setOnClickListener(this);

        AppInfo data = mData.get(position);
        Glide.with(allAppsViewHolder.ivItemAppsThumbnail).
                load(data.getIcon()).into(allAppsViewHolder.ivItemAppsThumbnail);
        allAppsViewHolder.tvItemAppsName.setText(data.getName());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_item_apps_root :
                if(view.getTag() == null) return;
                if(mCallback == null) return;

                int pos = (int) view.getTag();
                mCallback.onAppCardClicked(mData.get(pos));
                break;

            default:
                break;
        }
    }
}
