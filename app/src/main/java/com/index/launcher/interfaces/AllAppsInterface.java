package com.index.launcher.interfaces;

import com.index.launcher.objects.AppInfo;

public interface AllAppsInterface {
    void onAppCardClicked(AppInfo appInfo);
}
